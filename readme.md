## Markdown Table Formatter

A small Python script that reformats Markdown files containing |--- tables.

### Installation

```bash
sudo curl -L https://bitbucket.org/sdokara/markdown-table-formatter/raw/master/mtf -o /usr/local/bin/mtf
sudo chmod +x /usr/local/bin/mtf
```

### Usage

direct: 

```
mtf [-h] [-i] [-f FILE]

optional arguments:
  -h, --help            show this help message and exit
  -i, --in-place        reformat in-place, overwrite source
  -f FILE, --file FILE  source markdown file
```

stdin:

```bash
cat sample.md | mtf > formatted.md
```