#!/usr/bin/env python3
import sys
from argparse import ArgumentParser


class MarkdownTableFormatter:
    def __init__(self):
        self.output = ''
        self.column_sizes = []

    def format(self, lines):
        table = []
        for i in range(0, len(lines)):
            parts = lines[i].split('|')[:-1]
            row = [part.strip() for part in parts[1:]]
            if len(row) > 0:
                table.append(row)

        for i in range(0, len(table[0])):
            length = 0
            for j in range(0, len(table)):
                if j == 1:
                    continue
                cur = len(table[j][i])
                if cur > length:
                    length = cur
            self.column_sizes.append(length)

        self.append_row(table[0])
        self.append_separator()
        for i in range(2, len(table)):
            self.append_row(table[i])
        return self.output

    def append_row(self, row):
        self.append('| ')
        for i in range(0, len(row)):
            cell = row[i]
            padding = self.column_sizes[i] - len(cell)
            self.append(cell).append(' ')
            for j in range(0, padding):
                self.append(' ')
            if i < len(row) - 1:
                self.append('| ')
        self.append('|\n')

    def append_separator(self):
        self.append('|')
        for column_size in self.column_sizes:
            for i in range(0, column_size + 2):
                self.append('-')
            self.append('|')
        self.append('\n')

    def append(self, string):
        self.output += string
        return self


class MarkdownFileReformatter:
    def __init__(self, path=None):
        self.path = path if path else None
        self.output = ''

    def __enter__(self):
        if self.path:
            self.file = open(self.path, 'r')
        else:
            self.file = sys.stdin
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.path:
            self.file.close()

    def format(self):
        line = self.file.readline()
        while line:
            if not line.startswith('|'):
                self.output += line
                line = self.file.readline()
            else:
                output, line = self.format_table(line)
                self.output += output
        return self.output

    def format_table(self, line):
        lines = []
        while line and line.strip() != '':
            lines.append(line)
            line = self.file.readline()
        formatter = MarkdownTableFormatter()
        return formatter.format(lines), line


def main():
    parser = ArgumentParser()
    parser.add_argument('-i', '--in-place', dest='inplace', action='store_true',
                        help='reformat in-place, overwrite source')
    parser.add_argument('-f', '--file', dest='file', type=str, help='source markdown file')
    args = parser.parse_args()

    if args.file:
        with MarkdownFileReformatter(args.file) as formatter:
            output = formatter.format()
        if args.inplace:
            with open(args.file, 'w') as ofile:
                ofile.write(output)
        else:
            print(output, end='')
    else:
        with MarkdownFileReformatter() as formatter:
            output = formatter.format()
            print(output, end='')


if __name__ == '__main__':
    main()
